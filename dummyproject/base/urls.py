"""Base url configurations."""
from django.urls import path
from .views import (LandingPageTemplateView,
                    BasicDemoTemplateView)

app_name = "base"

urlpatterns = [
    path('', LandingPageTemplateView.as_view(), name="index"),
    # elements
    path('basic-demo', BasicDemoTemplateView.as_view(), name="basic-demo"),
]
