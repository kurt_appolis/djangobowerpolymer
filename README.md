# README #

A Django 2.# project using bower to get Polymer 2.0 displaying.


### How do I get set up? ###

> cd ./djangobowerpolymer

> virtualenv -p python3 ./env


> source ./env/bin/activate


> pip install -r ./requirements.txt


> cd ./dummyproject


> ./manage.py migrate


> ./manage.py createsuperuser


> ./manage.py bower install Polymer/polymer#^2.0.0


> ./manage.py collectstatic 


> ./manage.py runserver


### Resources ###

* https://github.com/nvbn/django-bower

* https://www.polymer-project.org/2.0/start/install-2-0