"""Base views."""
# from django.shortcuts import render
from django.views.generic import (TemplateView)


class LandingPageTemplateView(TemplateView):
    """Landing Page TemplateView."""

    template_name = "base/index.html"


class BasicDemoTemplateView(TemplateView):
    """Landing Page TemplateView."""

    template_name = "base/elements/basic-demo.html"
